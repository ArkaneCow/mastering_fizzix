#include <emscripten/bind.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

using namespace emscripten;

float solve_31_a(float temp_max, float temp_min, float work)
{
	float efficiency = (temp_max - temp_min)/temp_max;
	return work/efficiency;
}
float solve_31_b(float temp_max, float temp_min, float work)
{
	float efficiency = (temp_max - temp_min)/temp_max;
	float q_h = work/efficiency;
	return q_h - work;
}
float solve_30_a(float exhaust, float work)
{
	return work/(exhaust+work);
}
float solve_29_a(float q_h, float q_c)
{
	return q_h - q_c;
}
float solve_28_a(float pressure_kpa, float volume)
{
	return (pressure_kpa*1000)*volume;
}
float solve_28_b(float pressure_kpa, float volume)
{
	float new_volume = (1/3)*volume;
	return (new_volume - volume)*(pressure_kpa*1000);
}
float solve_27_a(float heat, float t_i, float t_f)
{
	float delta_u = (1.5*8.31)*(t_f - t_i);
	return heat - delta_u;
}
float solve_26_a(float work, float delta_u)
{
	return delta_u + work;
}
float solve_19_a(float mass, float temperature, float mu, float v_i, float v_f)
{
	float friction_force = mass*9.8*mu;
	float friction_acceleration = friction_force/mass;
	float distance = ((v_f*v_f) - (v_i*v_i))/(-2*friction_acceleration);
	float friction_work = friction_force*distance;
	float ke_i = 0.5*mass*(v_i*v_i);
	float ke_f = friction_work;
	return ke_f/((2108*-temperature)+334000);
}
float solve_18_b(float mass, float temperature, float heat)
{
	float q_1 = mass*2108*-temperature;
	float remain = heat - q_1;
	return mass - (remain/334000);
}
float solve_17_a(float mass)
{
	return mass*(207000);
}
float solve_16_a(float height_i, float height_f)
{
	return (100000*height_i)/height_f;
}
float solve_15_a(float oxygen_temp)
{
	return (oxygen_temp*2)/32;
}
float solve_14_a(float temp_i, float pressure_i, float pressure_f)
{
	return (pressure_f*1000*temp_i)/(pressure_i*1000);
}
float solve_13_a(float v_cm_i, float temp_c_i, float v_cm_f, float pressure_gauge)
{
	float cross = ((pressure_gauge+100000)*(273+27)*(v_cm_f/(100*100*100)))/(100000*(v_cm_i/(100*100*100)));
	return cross - 273;
}
float solve_7_a(float heat, float mass_g, float temp_c_i)
{
	float delta_t = heat/((mass_g/1000)*897);
	float t_f_c = delta_t + (temp_c_i+273);
	return t_f_c - 273;
}

EMSCRIPTEN_BINDINGS(fluids)
{
	function("solve_31_a", &solve_31_a);
	function("solve_31_b", &solve_31_b);
	function("solve_30_a", &solve_30_a);
	function("solve_29_a", &solve_29_a);
	function("solve_28_a", &solve_28_a);
	function("solve_28_b", &solve_28_b);
	function("solve_27_a", &solve_27_a);
	function("solve_26_a", &solve_26_a);
	function("solve_19_a", &solve_19_a);
	function("solve_18_b", &solve_18_b);
	function("solve_17_a", &solve_17_a);
	function("solve_16_a", &solve_16_a);
	function("solve_15_a", &solve_15_a);
	function("solve_14_a", &solve_14_a);
	function("solve_13_a", &solve_13_a);
	function("solve_7_a", &solve_7_a);
}