

#include <emscripten/bind.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

using namespace emscripten;

int digits = 7;

//physics constants
float earth_gravity = 9.8;
float pi = 3.14159;

//kg/m^3
float density_olive_oil = 918;
float density_water = 1000;
float density_mercury = 13534;
float density_lead = 11340;
float density_air = 1.2;
float density_blood = 1025;

//Pa
float atmosphere_pressure = 100000;

float pressure_fluid_rest(float density, float height)
{
	return density*earth_gravity*height; 
}
float delta_height_u_tube(float diameter_left_cm, float mass_left, float diameter_right_cm, float mass_right, float fluid_density)
{
	float radius_left = (diameter_left_cm/100) /2;
	float radius_right = (diameter_right_cm/100) /2;
	float area_left = pi*radius_left*radius_left;
	float area_right = pi*radius_right*radius_right;
	float pressure_left = (mass_left*earth_gravity)/area_left;
	float pressure_right = (mass_right*earth_gravity)/area_right;
	return fabsf((pressure_left/(earth_gravity*fluid_density)) - ((pressure_right)/(earth_gravity*fluid_density)));
}
float weight_sink_depth(float width, float height, float depth_cm)
{
	float volume = width*height*(depth_cm/100);
	return (density_water*volume)*earth_gravity;
}
float volume_above_water(float mass, float total_volume)
{
	return total_volume - (mass/density_water);
}
float lead_puck_mercury_bath_sink_depth(float diameter_cm, float height_cm)
{
	float radius = ((diameter_cm/100)/2);
	return ((((radius*radius*pi*(height_cm/100))*density_lead)/density_mercury)/(radius*radius*pi))*100;	
}
float pool_fill(float hose_diameter_cm, float hose_speed, float pool_diameter, float pool_depth_cm)
{
	return ((pool_diameter/2)*(pool_diameter/2)*pi*(pool_depth_cm/100))/((hose_diameter_cm/200)*(hose_diameter_cm/200)*pi*hose_speed);
}
float flow_rate(float flow_speed, float diameter_cm)
{
	float radius = (diameter_cm/2)/100;
	float area = radius*radius*pi;
	float volume_time = area*flow_speed;
	return volume_time*density_water;
}
float flow_diameter_decrease(float diameter_old_cm, float flow_old, float diameter_new_cm)
{
	return (((diameter_old_cm/2)/100)*((diameter_old_cm/2)/100)*pi*flow_old)/(((diameter_new_cm/2)/100)*((diameter_new_cm/2)/100)*pi);
}
float airplane_lift(float velocity_top, float velocity_bottom)
{
	return (0.5*density_air*((velocity_top*velocity_top)-(velocity_bottom*velocity_bottom)))/1000;
}
float blood_flow_plaque(float diameter_first_cm, float diameter_second_cm, float flow_rate_cm)
{
	float radius_1 = (diameter_first_cm/100)/2;
	float radius_2 = (diameter_second_cm/100)/2;
	float flow_rate = flow_rate_cm / 100;
	return ((radius_1*radius_1*pi*flow_rate)/(pi*radius_2*radius_2))*100;
}
float delta_blood_pressure(float flow_rate_first_cm, float flow_rate_second_cm)
{
	return 0.5*density_blood*((flow_rate_second_cm/100)*(flow_rate_second_cm/100) - (flow_rate_first_cm/100)*(flow_rate_first_cm/100));
}
float horizontal_tube_velocity(float pressure_delta_kpa, float diameter_first_cm, float diameter_second_cm)
{
	float v_2_coefficient = ((diameter_first_cm/200)*(diameter_first_cm/200)*pi)/((diameter_second_cm/200)*(diameter_second_cm/200)*pi);
	return sqrt((pressure_delta_kpa*1000)/((v_2_coefficient*v_2_coefficient - 1)*500));
}
float air_speed_raise_water(float height_cm)
{
	return sqrt((density_water*earth_gravity*(height_cm/100))/(0.5*density_air));
}
float height_efflux(float distance_bottom_one, float distance_bottom_two)
{
	float h_coefficient = distance_bottom_one - distance_bottom_two;
	return ((distance_bottom_one*distance_bottom_one)-(distance_bottom_two*distance_bottom_two))/h_coefficient;
}
float solve_1_a(float depth_water_cm, float depth_olive_oil_cm)
{
	return pressure_fluid_rest(density_olive_oil, depth_olive_oil_cm/100) + pressure_fluid_rest(density_water, depth_water_cm/100);
}
float solve_4_a(float diameter_left_cm, float mass_left, float diameter_right_cm, float mass_right, float fluid_density)
{
	return delta_height_u_tube(diameter_left_cm, mass_left, diameter_right_cm, mass_right, fluid_density);
}
float solve_8_a(float width, float height, float depth_cm)
{
	return weight_sink_depth(width, height, depth_cm);
}
float solve_9_a(float mass, float total_volume)
{
	return volume_above_water(mass, total_volume);
}
float solve_9_b(float increase_volume)
{
	return earth_gravity*increase_volume*density_water;
}
float solve_10_a(float diameter_cm, float height_cm)
{
	return lead_puck_mercury_bath_sink_depth(diameter_cm, height_cm);
}
float solve_11_a(float hose_diameter_cm, float hose_speed, float pool_diameter, float pool_depth_cm)
{
	return pool_fill(hose_diameter_cm, hose_speed, pool_diameter, pool_depth_cm);
}
float solve_12_a(float flow_speed, float diameter_cm)
{
	return flow_rate(flow_speed, diameter_cm);
}
float solve_13_a(float diameter_old_cm, float flow_old, float diameter_new_cm)
{
	return flow_diameter_decrease(diameter_old_cm, flow_old, diameter_new_cm);
}
float solve_14_a(float velocity_top, float velocity_bottom)
{
	return airplane_lift(velocity_top, velocity_bottom);
}
float solve_14_b(float pressure_kpa, float area_wing)
{
	return (pressure_kpa*area_wing);
}
float solve_16_a(float diameter_first_cm, float diameter_second_cm, float flow_rate_cm)
{
	return blood_flow_plaque(diameter_first_cm, diameter_second_cm, flow_rate_cm);
}
float solve_16_b(float flow_first, float flow_second)
{
	return delta_blood_pressure(flow_first, flow_second);
}
float solve_17_c(float pressure_delta_kpa, float diameter_first_cm, float diameter_second_cm)
{
	return horizontal_tube_velocity(pressure_delta_kpa, diameter_first_cm, diameter_second_cm);
}
float solve_18_a(float height_cm)
{
	return air_speed_raise_water(height_cm);
}
float solve_19_a(float surface_distance)
{
	return sqrt(2*earth_gravity*surface_distance);
}
float solve_20_a(float distance_bottom_one, float distance_bottom_two)
{
	return height_efflux(distance_bottom_one, distance_bottom_two);
}

EMSCRIPTEN_BINDINGS(fluids)
{
	function("solve_1_a", &solve_1_a);
	function("solve_4_a", &solve_4_a);
	function("solve_8_a", &solve_8_a);
	function("solve_9_a", &solve_9_a);
	function("solve_9_b", &solve_9_b);
	function("solve_10_a", &solve_10_a);
	function("solve_11_a", &solve_11_a);
	function("solve_12_a", &solve_12_a);
	function("solve_13_a", &solve_13_a);
	function("solve_14_a", &solve_14_a);
	function("solve_14_b", &solve_14_b);
	function("solve_16_a", &solve_16_a);
	function("solve_16_b", &solve_16_b);
	function("solve_17_c", &solve_17_c);
	function("solve_18_a", &solve_18_a);
	function("solve_19_a", &solve_19_a);
	function("solve_20_a", &solve_20_a);
}

