using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

namespace gen_site
{
	class MainClass
	{
		static string html = "";
		public static void Main (string[] args)
		{
			if (args.Length != 2) {
				Console.WriteLine ("wrong arguments");
				Environment.Exit (-1);
			}
			Console.WriteLine ("Hello World!");
			string site_name = args [0];
			Console.WriteLine ("site_name is " + site_name);
			string source_file = args [1];
			Console.WriteLine ("source_file set to " + source_file);
			StreamReader sr = new StreamReader (source_file);
			string source_source = sr.ReadToEnd ();
			sr.Close ();
			string source_file_name = Regex.Replace (source_file, ".cpp", "");
			string source_file_js = source_file_name += ".js";

			string html_end = "</html>";

			string[] source_line = source_source.Split ('\n');
			//test
			for (int l = 0; l < source_line.Length; l++) {
				Console.WriteLine (source_line [l]);
			}
			List<string> function_names = new List<string> ();
			List<List<string>> function_parameters = new List<List<string>> ();
			for (int i = 0; i < source_line.Length; i++) {
				try {
					if (source_line[i].Contains("float") && source_line[i].Contains(";") == false)
					{
						function_names.Add (source_line[i].Split(' ')[1].Split('(')[0]);
						string[] f_params = source_line[i].Split('(')[1].Split(')')[0].Split(',');
						List<string> f_param_list = new List<string>();
						for (int k = 0; k < f_params.Length; k++)
						{
							f_param_list.Add(Regex.Split(f_params[k], "float")[1].Trim());
						}
						function_parameters.Add(f_param_list);
					}
				}
				catch (Exception ex) {
					Console.WriteLine (ex.ToString ());
				}
			}
			int smallest = 9001;
			char smallest_part = 'z';
			for (int j = 0; j < function_names.Count; j++) {
				string parse_num = function_names [j].Split ('_') [1];
				int ques_num = Int32.Parse (parse_num);
				if (ques_num < smallest) {
					smallest = ques_num;
					smallest_part = function_names [j].Split ('_') [2][0];
				}
			}
			string html_calculate = "<script type=\"text/javascript\">\n";
			for (int p = 0; p < function_names.Count; p++) {
				string variables = "";
				for (int q = 0; q < function_parameters[p].Count; q++) {
					variables += "var " + function_parameters [p][q] + " = document.getElementById(\"" + function_parameters [p][q] + "\").value;\n"; 
				}
				string answer = "var ans = Module." + function_names [p] + "(";
				for (int r = 0; r < function_parameters[p].Count; r++) {
					answer += "parseFloat(" + function_parameters [p] [r] + "),";
				}
				answer = answer.Substring (0, answer.Length - 1);
				answer += ");\n";
				string set_ans = "document.getElementById(\"answer\").innerHTML = ans;\n";
				html_calculate += "function " + Regex.Replace (function_names [p], "solve_", "calculate_") + "()\n{\n" + variables + "\n" + (Regex.Replace((answer + "\n"), "\"", "\\\"")) +"\n" + set_ans + "\n}\n";
			}

			string first_function = "load_" + smallest + "_" + smallest_part.ToString() + "()";
			string html_content_default = "<div class=\"hero-unit\"><h1>" + site_name + "</h1><p>Physics " + site_name.ToLower() + " equations written in C++ then compiled to javascript using Emscripten. HTML generated with C#...</p><a href=\"#\" class=\"btn btn-primary btn-lg\" onclick=\"" + first_function + "\">Fabrice</a></div>";
			string html_ui = "";
			string html_load = "\n";
			html_load += "function load_default()\n{\nvar content = \""+ Regex.Replace(html_content_default, "\"", "\\\"") +"\";\ndocument.getElementById(\"content\").innerHTML = content;\n}\n";
			for (int m = 0; m < function_names.Count; m++) {
				string input = "";
				for (int n = 0; n < function_parameters[m].Count; n++) {
					input += "<input type=\"text\" id=\""+ function_parameters[m][n] +"\" />";
				}
				string answer = "<p><div id=\"answer\"></div></p>";
				string title = function_names [m].Split ('_') [1] + " " + function_names[m].Split('_')[2];
				string solve_button = "<button type=\"button\" class=\"btn btn-default\" onclick=\""+ Regex.Replace(function_names[m], "solve_", "calculate_") + "()\">Calculate</button>";
				html_load += "function "+ Regex.Replace(function_names[m], "solve_", "load_") +"()\n{\nvar content = \"" + Regex.Replace((title + " " + input + " " + solve_button + " " + answer), "\"", "\\\"") +"\";\ndocument.getElementById(\"content\").innerHTML = content;\n}\n";
			}
			html_load += "\n";
			html_calculate += html_load + "\n</script>\n";;
			string navlist = "<ul class=\"nav nav-list\">\n<li class=\"nav-header\">" + site_name + "</li>\n<li class=\"active\"><a href=\"#\" onclick=\"load_default()\">" + site_name + "</a></li>\n";
			for (int s = 0; s < function_names.Count; s++) {
				navlist += "<li><a href=\"#\" onclick=\"" + Regex.Replace(function_names[s], "solve_", "load_") + "() \">" + function_names[s].Split('_')[1] + " " + function_names[s].Split('_')[2] + "</a></li>\n";
			}
			navlist += "\n</ul>\n";
			html_ui = "<div class=\"container\">\n<h1><a href=\"#\" onclick=\"load_default()\">Fizzix</a></h1>\n<div class=\"row\">\n" + "<div class=\"col-md-4 col-lg-4\">\n" + navlist + "</div><div class=\"col-md-8 col-lg-8\"><div id=\"content\">\n" + html_content_default + "\n</div>" +"\n</div></div></div>";
			string html_begin = "<!DOCTYPE html>\n<html><head>\n<meta charset=\"utf-8\">\n<title>" + site_name + "</title>\n<link rel=\"stylesheet\" href=\"css/metro-bootstrap.css\" type=\"text/css\"/>\n" + html_calculate + "\n<script src=\"jquery/jquery-1.10.2.js\"></script>\n<script src=\"src/" + source_file_js + "\"></script>\n<script src=\"boostrap/js/bootstrap.js\"></script></head>";
			html += html_begin + "\n";
			html += html_ui;
			html += html_end;
			string output_name = site_name + ".html";
			if (File.Exists (output_name)) {
				File.Delete (output_name);
			}
			StreamWriter html_write = new StreamWriter (output_name);
			html_write.Write (html);
			html_write.Close ();
			/*
			FileInfo html_file = new FileInfo (output_name);
			if (File.Exists (html_file.Directory.Parent.FullName + "\\" + html_file.Name)) {
				File.Delete (html_file.Directory.Parent.FullName + "\\" + html_file.Name);
			}
			html_file.CopyTo (html_file.Directory.Parent.FullName + "\\" + html_file.Name);
			File.Delete (output_name);
			*/
		}
	}
}
